var express = require('express');
var app = express();
var bodyParser = require('body-parser');



app.use(express.static(__dirname + '/public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.post('/form', function (req,res) {
    console.log('success');
    console.log(req.body);
    // var data = req.body.formObj.length;
    // console.log(data);
    // res.status(200);
    res.redirect(200,'/thankyou.html')
});


// app.post('/form', function (req, res, next) {
//     console.log(req.body.formObj);
//     res.json(req.body.formObj);
// });


var portNumber = process.argv[2] || 3000;
app.listen(portNumber, function () {
    console.info('Webserver started on port %d', portNumber)
});