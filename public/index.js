angular.module('formApp', [])
.controller('formCtrl', myCtrl);

function myCtrl($http) {
    var vm = this;
    vm.email ="";
    vm.pw ="";
    vm.name ="";
    vm.gender ="";
    vm.dob = "";
    vm.add = "";
    vm.pc = "";
    vm.cty ="";
    vm.cn ="";

    vm.formObj = [];

    vm.createForm = function () {
        
        var obj = {};
        // console.info('please load'   );
        obj['email'] = vm.email;
        obj['password'] = vm.pw;
        obj['name'] = vm.name;
        obj['gender'] = vm.gender;
        obj['dob'] = vm.dob;
        obj['address'] = vm.add;
        obj['postalCode'] = vm.pc;
        obj['country'] = vm.cty;
        obj['contactNumber'] = vm.cn;
        
        vm.formObj.push(obj);

        vm.email ="";
        vm.pw ="";
        vm.name ="";
        vm.gender ="";
        vm.dob = "";
        vm.add = "";
        vm.cty ="";
        vm.cn ="";

        $http.post('/form', JSON.stringify(vm.formObj))
            .then(function (req,res) {
                // console.log(req.body + res)
                res.redirect('/thankyou.html')
            }).catch(function (err) {
            console.log(err);
        });



    };

}


